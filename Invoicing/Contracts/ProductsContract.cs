﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Invoicing.Contracts
{
    public class ProductsContract
    {
        public class Post
        {
            public class Request
            {
                public string Name { get; set; }
                public string Description { get; set; }
                public decimal Price { get; set; }
                public int Stock { get; set; }
            }

        }
    }
}
