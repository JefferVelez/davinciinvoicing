﻿using Invoicing.Contracts;
using Invoicing.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Invoicing.Controllers
{
    [ApiController]
    [Route("api/invoices")]
    public class InvoicesController : ControllerBase
    {
        private readonly InvoicingDbContext context;

        public InvoicesController(InvoicingDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<Invoice>>> Get()
        {
            return await context.Invoices.Include(x => x.InvoiceDetails).ToListAsync();
        }

        [HttpGet("{Id:Int}")]
        public async Task<ActionResult<List<Invoice>>> Get(int Id)
        {
            return await context.Invoices.Where(x => x.Id == Id).Include(x => x.InvoiceDetails).ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult> Post(InvoicesContract.PostInvoice.RequestInvoice request)
        {
            try
            {
                var invoice = new Invoice()
                {
                    CustomerId = request.CustomerId,
                    Date = request.Date,
                };

                var productsIds = request.Details.Select(d => d.ProductId).ToArray();
                var products = context.Products.Where(p => productsIds.Contains(p.Id)).ToList();

                int rowNumber = 0;
                var invoiceDetails = new List<InvoiceDetail>();

                if (products.Count == 0) return BadRequest($"Products in request.detail where no found.");

                foreach (var detail in request.Details)
                {
                    var product = products.Where(p => p.Id == detail.ProductId).First();

                    if ((product.Stock - detail.Quantity) < 0)
                    {
                        return BadRequest($"No Stock avail for product: '{detail.ProductId}' Stock: {product.Stock}");
                    }

                    var invoiceDetail = new InvoiceDetail()
                    {
                        Price = product.Price,
                        ProductId = detail.ProductId,
                        Quantity = detail.Quantity,
                        RowNumber = ++rowNumber,
                        TaxValue = product.Price * 0.16m
                    };

                    //adding details
                    invoiceDetails.Add(invoiceDetail);
                    //subtract product stock
                    product.Stock -= invoiceDetail.Quantity;
                }

                //filling invoice header's totals values according invoice's details sum
                invoice.TaxValue = invoiceDetails.Sum(id => id.TaxValue);
                invoice.Quantity = invoiceDetails.Sum(id => id.Quantity);
                invoice.SubTotal = invoiceDetails.Sum(id => id.SubTotal);

                using (var tran = context.Database.BeginTransaction())
                {
                    context.Invoices.Add(invoice);
                    await context.SaveChangesAsync();
                    //InvoiceId generated in DB

                    //adding details
                    foreach (var id in invoiceDetails)
                    {
                        id.InvoiceId = invoice.Id;
                        context.InvoiceDetails.Add(id);
                    }

                    //updating product info.
                    foreach (var product in products)
                    {
                        context.Entry(product).State = EntityState.Modified;
                    }

                    await context.SaveChangesAsync();
                    tran.Commit();
                }

                return Ok(new { InvoiceId = invoice.Id });

            }
            catch (Exception ex)
            {
                return BadRequest($"Error: '{ex.InnerException.InnerException.Message}'");

            }
        }

        [HttpDelete("{Id:Int}")]     
        public async Task<ActionResult> Delete(int Id)
        {
            //Get the Invoice from the model
            var invoice = context.Invoices.Find(Id);

            //get the invoice's details
            var invoiceDetails = context.InvoiceDetails.Where(d => d.InvoiceId == Id).ToList();

            //Get the ids from details
            var productsIds = invoiceDetails.Select(d => d.ProductId).ToArray();

            //Listing all the details from the model based on the ids.
            var products = context.Products.Where(p => productsIds.Contains(p.Id)).ToList();

            foreach (var d in invoiceDetails)
            {
                //finding the product
                var product = products.Where(p => p.Id == d.ProductId).First();

                //sum stock.
                product.Stock += d.Quantity;
                context.Entry(product).State = EntityState.Modified;

            }

            context.Entry(invoice).State = EntityState.Deleted;
            await context.SaveChangesAsync();
            return Ok();

        }        

        [HttpPut("{Id:Int}")]
        public async Task<ActionResult> Put(Invoice invoice, int Id)
        {
            var exists = await context.Invoices.AnyAsync(x => x.Id == Id);

            if (!exists)
            {
                return NotFound("Invoice ID Doesnt exists");
            }

            context.Update(invoice);
            await context.SaveChangesAsync();
            return Ok();

        }


    }
}
