﻿using Invoicing.Contracts;
using Invoicing.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Invoicing.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class ProductsController : ControllerBase
    {

        private readonly InvoicingDbContext context;

        public ProductsController(InvoicingDbContext context)
        {
            this.context = context;
        }


        [HttpGet]
        public async Task<ActionResult<List<Product>>> Get()
        {
            return await context.Products.ToListAsync();

        }

        [HttpGet("{Id:Int}")]
        public async Task<ActionResult<List<Product>>> Get(int Id)
        {
            return await context.Products.Where(x => x.Id == Id).ToListAsync();

        }

        [HttpPost]
        public async Task<ActionResult> Post(ProductsContract.Post.Request request)
        {
            var product = new Product()
            {
                Name = request.Name,
                Description = request.Description,
                Price = request.Price,
                Stock = request.Stock
            };

            context.Products.Add(product);
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{Id:Int}")]
        public async Task<ActionResult> Delete(int Id)
        {
            var exists = await context.Products.AnyAsync(x => x.Id == Id);

            if (!exists)
            {
                return NotFound("ID Doesnt exists");
            }

            context.Remove(new Product() { Id = Id });
            await context.SaveChangesAsync();
            return Ok();
        }


        [HttpPut("{Id:Int}")]  
        public async Task<ActionResult> Put(Product prod, int Id)
        {

            var product = context.Products.Where(p => p.Id == Id).FirstOrDefault();

            if (product == null) return BadRequest($"ProductId: '{prod.Id}' Not Found");

            product.Id = Id;
            product.Name = prod.Name != null ? prod.Name : product.Name;
            product.Description = prod.Description != null ? prod.Description : product.Description;
            product.Price = prod.Price != 0 ? prod.Price : product.Price;
            product.Stock = prod.Stock != 0 ? prod.Stock : product.Stock;


            context.Update(product);
            await context.SaveChangesAsync();
            return Ok();

        }


    }
}
