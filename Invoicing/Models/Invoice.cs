﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Invoicing.Models
{
    [Table("Invoice")]
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public int Quantity { get; set; }
        public decimal TaxValue { get; set; }
        public decimal SubTotal { get; set; }

        [NotMapped]
        public decimal Total { get { return TaxValue + SubTotal; } }
        public List<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
