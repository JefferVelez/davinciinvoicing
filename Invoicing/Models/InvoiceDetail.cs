﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Invoicing.Models
{
    [Table("InvoiceDetail")]
    public class InvoiceDetail
    {   
        public int InvoiceId { get; set; }
        public int RowNumber { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal TaxValue { get; set; }

        [NotMapped]
        public decimal SubTotal { get { return Price * Quantity; } }

        [NotMapped]
        public decimal Total { get { return SubTotal + TaxValue; } }
     
    }
}
